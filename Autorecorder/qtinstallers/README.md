# QtInstallers

Application installer creator for Qt applications. Creates installer for Windows and appimage for Linux systems.

# Dependencies 

Windows 

- Windows 10
- NSIS 3 (https://nsis.sourceforge.io/Download), tool added to system path environment (i.e. accessible from cmd)
- 7zip (https://www.7-zip.org/download.html), tool added to system path environment (i.e. accessible from cmd)

Linux 

- Ubuntu 16.0x environment
- Linuxdeployqt (https://github.com/probonopd/linuxdeployqt), tool added to .profile (i.e. accessible from terminal)

# Software to be deployed requirerements
Definition: <root> == root level of software project directory.
Application has deployment directory. Deployment is done there under <root>/Deployment/<platform ("Windows","LinuxUbuntu")> 
Application source directory follow following structure

## Sources:
<root>/src/<stuff>

## Installer gfx 
<root>/src/gfx

## Licence text file 
<root>/licence.txt

## QML stuff:
<root>/src/ui/

## Gfx
<root>/src/gfx/applicationIcons
	<projectName>.ico for Windows installer
	<projectName>.svg for Linux installer

<root>/src/gfx/productLogo
	<projectName>.bmp for Windows installer
	<projectName>.svg for Linux installer

## Docs (pdf and txt files): 
<root>/docs 

# Usage 

## Integrate to Qt creator as custom build step 
This has been tested on MinGW compiler. 

When creating installer, create "ReleaseAndDeploy" build configuration. Create copy from Release for example. 
Add custom build step
-- command: 			cpack 
-- arguments: 			-C Release
-- working directory: 	%{buildDir}

If you get "Warning: Cannot find GCC installation directory. g++.exe must be in the path.",
copy g++ > C:\Qt\5.12.6\mingw73_64\bin to qt https://forum.qt.io/topic/88525/windeployqt-exe-does-not-copy-all-needed-files/9

