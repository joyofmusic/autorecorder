#!/bin/bash
TARGETBINARYFILENAME=$1
BUILDDIRECTORY=$2
TARGETSOURCEPATH=$3
LINUXDEPLOYTOOL=$4
echo "inputs" $TARGETBINARYFILENAME $BUILDDIRECTORY $TARGETSOURCEPATH $LINUXDEPLOYTOOL


#Create appimage directory 
APPIMAGEDIR=$BUILDDIRECTORY/$TARGETBINARYFILENAME.AppDir

if [ -d "$APPIMAGEDIR" ]; then 
	rm -Rf $APPIMAGEDIR; 
fi

mkdir -p $APPIMAGEDIR
mkdir -p $APPIMAGEDIR/usr/bin
mkdir -p $APPIMAGEDIR/usr/lib
mkdir -p $APPIMAGEDIR/usr/share/applications
mkdir -p $APPIMAGEDIR/usr/share/icons/hicolor/48x48/apps

#Copy needed stuff into directory 
#https://github.com/probonopd/linuxdeployqt 
## binary
cp $TARGETBINARYFILENAME $APPIMAGEDIR/usr/bin/$TARGETBINARYFILENAME


##icons etc 
cp $TARGETSOURCEPATH/Deployment/LinuxUbuntu/assets/$TARGETBINARYFILENAME.desktop $APPIMAGEDIR/usr/share/applications
cp $TARGETSOURCEPATH/src/gfx/applicationIcons/$TARGETBINARYFILENAME.svg $APPIMAGEDIR/usr/share/icons/hicolor/48x48/apps/$TARGETBINARYFILENAME.svg

##invoke appimage too
cd $BUILDDIRECTORY
$LINUXDEPLOYTOOL $APPIMAGEDIR/usr/share/applications/*.desktop -extra-plugins=iconengines,imageformats,platformthemes/libqgtk3.so -bundle-non-qt-libs -qmake=$QTDIR/bin/qmake -always-overwrite -qmldir=$TARGETSOURCEPATH/src/ui/ -appimage -verbose=0

#after creating it, remove original binary
rm $BUILDDIRECTORY/$TARGETBINARYFILENAME

#rename created app image to $TARGETBINARYFILENAME
#TimeToPic--x86_64.AppImage --> TimeToPic
mv $BUILDDIRECTORY/$TARGETBINARYFILENAME--x86_64.AppImage $BUILDDIRECTORY/$TARGETBINARYFILENAME

