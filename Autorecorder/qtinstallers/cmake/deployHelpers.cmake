# reference to https://github.com/nitroshare/nitroshare-desktop/blob/226dd32879981c0bf2ef4c78faee3af3b797bfdc/cmake/Windeployqt.cmake
# copy g++ > C:\Qt\5.12.6\mingw73_64\bin to qt https://forum.qt.io/topic/88525/windeployqt-exe-does-not-copy-all-needed-files/9

if(WIN32)
    ########################################################################
    # CPack configuration
    ########################################################################

    get_target_property(_qmake_executable Qt5::qmake IMPORTED_LOCATION)
    get_filename_component(_qt_bin_dir "${_qmake_executable}" DIRECTORY)
    find_program(WINDEPLOYQT_EXECUTABLE windeployqt HINTS "${_qt_bin_dir}")

    # Add commands that copy the Qt runtime to the target's output directory after
	# build and install the Qt runtime to the specified directory
        function(windeployqt target directory sourceDirectory)

            # Run windeployqt immediately after build
            add_custom_command(TARGET ${TARGETNAME} POST_BUILD
                    COMMAND "${CMAKE_COMMAND}" -E
                            env PATH="${_qt_bin_dir}" "${WINDEPLOYQT_EXECUTABLE}"
                                    --verbose 0
                                    --release
                                    --no-angle
                                    --compiler-runtime
                                    --qmldir ${sourceDirectory}/src/ui
                                    --no-opengl-sw
                                    \"$<TARGET_FILE:${target}>\"
            )

            # install(CODE ...) doesn't support generator expressions, but
            # file(GENERATE ...) does - store the path in a file
            file(GENERATE OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}_path"
                    CONTENT "$<TARGET_FILE:${target}>"
            )

            # Before installation, run a series of commands that copy each of the Qt
            # runtime files to the appropriate directory for installation
            install(CODE
                    "
                    file(READ \"${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}_path\" _file)
                    execute_process(
                            COMMAND \"${CMAKE_COMMAND}\" -E
                                    env PATH=\"${_qt_bin_dir}\" \"${WINDEPLOYQT_EXECUTABLE}\"
                                            --dry-run
                                            --release
                                            --no-angle
                                            --no-opengl-sw
                                            --compiler-runtime
                                            --qmldir ${sourceDirectory}/src/ui
                                            --list mapping
                                            \${_file}
                            OUTPUT_VARIABLE _output
                            OUTPUT_STRIP_TRAILING_WHITESPACE
                    )
                    separate_arguments(_files WINDOWS_COMMAND \${_output})
                    while(_files)
                            list(GET _files 0 _src)
                            list(GET _files 1 _dest)
                            execute_process(
                                    COMMAND \"${CMAKE_COMMAND}\" -E
                                            copy \${_src} \"\${CMAKE_INSTALL_PREFIX}/${directory}/\${_dest}\"
                            )
                            list(REMOVE_AT _files 0 1)
                    endwhile()
                    "
            )

	endfunction()

	mark_as_advanced(WINDEPLOYQT_EXECUTABLE)
	
else()    
    find_program(LINUXDEPLOYQT_EXECUTABLE linuxdeployqt-6-x86_64.AppImage HINTS "${DEPLOYTOOLSROOT}/linuxTools")

    function(linuxdeployqt target buildDirectory sourceDirectory)

            # Run windeployqt immediately after build
            add_custom_command(TARGET ${TARGETNAME} POST_BUILD
                    COMMAND "${CMAKE_COMMAND}" -E
                            env \"${DEPLOYTOOLSROOT}/linuxTools/createAppImage.sh\" ${target} ${buildDirectory} ${sourceDirectory} ${LINUXDEPLOYQT_EXECUTABLE}
            )

    endfunction()
    mark_as_advanced(LINUXDEPLOYQT_EXECUTABLE)

endif()
