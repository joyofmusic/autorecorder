/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QDebug>
#include <QtCore/QCoreApplication>
//
#include "practiceDiary.h"

const char *iniTrainingKey = "trainingtimeSecs";

elapsedTime_t PracticeDiary::getMSecondsExercisedSinceStart() {
    bool conversionOk;
    mSecondsExercisedSinceStart = mSettings.value(iniTrainingKey, 0).toUInt(&conversionOk);
    if (!conversionOk) {
        mSecondsExercisedSinceStart = 0;
    }
    return mSecondsExercisedSinceStart;
}

void PracticeDiary::setMSecondsExercisedSinceStart(quint32 secondsExercisedSinceStart) {
    mSecondsExercisedSinceStart = secondsExercisedSinceStart;
    mSettings.setValue(iniTrainingKey, mSecondsExercisedSinceStart);
}

void PracticeDiary::addSecondsToTrainingDiary(quint32 trainingSeconds) {
    auto newValue = getMSecondsExercisedSinceStart() + trainingSeconds;
    setMSecondsExercisedSinceStart(newValue);
}

PracticeDiary::PracticeDiary() : mSecondsExercisedSinceStart(0), mSettings(QSettings::IniFormat, QSettings::UserScope,
                                                                           QCoreApplication::organizationName(),
                                                                           QCoreApplication::applicationName()) {}
