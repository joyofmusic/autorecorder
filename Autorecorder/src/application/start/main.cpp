/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
//
#include "recorderbackend.h"

int main(int argc, char *argv[]) {
    /* Enable high dpi scaling to windows dialogs. Do this before creating application instance */
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    /* Set window icon. Some reason icon init should be right after app object creation.
       Otherwise linux version icon does not work. */

#if (defined (_WIN32) || defined (_WIN64))
    QIcon myIcon(":/../gfx/applicationIcons/Autorecorder.ico");
#elif (defined (LINUX) || defined (__linux__))
    QIcon myIcon(":/../gfx/applicationIcons/Autorecorder.svg");
#endif
    QGuiApplication::setWindowIcon(myIcon);

    qmlRegisterType<RecorderBackend>(
            "com.herwoodtechnologies.autorecorder.backend",
            1, 0, "RecorderBackend");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/autorecorder/Main.qml"));
    Q_ASSERT(url.isValid());
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated, &app,
                     [url](QObject *obj, const QUrl &objUrl) {
                         if (!obj && url == objUrl)
                             QCoreApplication::exit(-1);
                     },
                     Qt::QueuedConnection);
    engine.load(url);

    int returnCode = QGuiApplication::exec();
    return returnCode;
}
