/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include <QAudioBuffer>
#include <QAudioDeviceInfo>
#include <QDebug>
#include <QDir>
#include <QUrl>
#include <QtMath>
//
#include "audiomanager.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

AudioManager::AudioManager(QObject *parent)
        : QObject(parent),
          mLocalFile(QUrl::fromLocalFile(QDir::currentPath() + "/recording")) {

    selectAudioDevices();

    mSoundInitRetryTimer = QSharedPointer<QTimer>(new QTimer, &QObject::deleteLater);
    connect(mSoundInitRetryTimer.data(), &QTimer::timeout, this,
            &AudioManager::onSoundInitRetryTimerTimeOut);

    const int minDiffForRecordDb = 10;
    setMinVolumeDifferenceDbBeforeStartingRecord(minDiffForRecordDb);

    resetAudioLevels();
}

bool AudioManager::selectAudioDevices() {
    bool ret = false;
    auto outputDeviceList =
            QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    auto inputDeviceList = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);

    if (!outputDeviceList.empty() && !inputDeviceList.empty()) {
        mAudioOutputDevice = outputDeviceList.first();
        mAudioInputDevice = inputDeviceList.first();
        ret = true;
    }
    return ret;
}

int AudioManager::minVolumeDifferenceDbBeforeStartingRecord() const {
    return m_minVolumeDifferenceDbBeforeStartingRecord;
}

void AudioManager::start() {
    createTimers();
    setupAudio();
}

bool AudioManager::setupAudio() {
    bool ret = false;
    if (initializeAudio()) {
        Q_ASSERT(mAudioInputCheckerTimer.data());
        mAudioInputCheckerTimer->start(audioCheckerCheckIntervalMs);
        ret = true;
    } else {
    }
    setAudioIsGood(ret);
    return ret;
}

void AudioManager::createTimers() {

    mAudioInputCheckerTimer = QSharedPointer<QTimer>(new QTimer, &QObject::deleteLater);
    connect(mAudioInputCheckerTimer.data(), &QTimer::timeout, this,
            &AudioManager::onAudioInputCheckerTimerTimeout);

    mAudioOutputCheckerTimer = QSharedPointer<QTimer>(new QTimer, &QObject::deleteLater);
    connect(mAudioOutputCheckerTimer.data(), &QTimer::timeout, this,
            &AudioManager::onAudioOutputCheckerTimerTimeout);

    mSoundSilenceCheckTimer = QSharedPointer<QTimer>(new QTimer, &QObject::deleteLater);
    connect(mSoundSilenceCheckTimer.data(), &QTimer::timeout, this,
            &AudioManager::onSoundSilenceCheckTimerTimeOut);

    mAudioPlaybackVolumeController = QSharedPointer<QTimer>(new QTimer, &QObject::deleteLater);
    connect(mAudioPlaybackVolumeController.data(), &QTimer::timeout, this,
            &AudioManager::onPlayBackVolumeTimerAdjustTimeOut);

}

void AudioManager::stop() {
    removeTimers();
    shutdownAudio();
    setAudioIsGood(false);
}

void AudioManager::removeTimers() {
    mSoundSilenceCheckTimer.clear();
    mAudioPlaybackVolumeController.clear();
    mAudioInputCheckerTimer.clear();
    mAudioOutputCheckerTimer.clear();
}

void AudioManager::shutdownAudio() {
    mAudioOutput->stop();
    mAudioInput->stop();
    mAudioOutput.clear();
    mAudioInput.clear();
    mAudioGenerator.clear();
    mAudioReceiver.clear();
}

void AudioManager::startRecording() {
    mDetectionMode = detectingRecordingTrigger;
}

void AudioManager::stopRecording() { mDetectionMode = paused; }

void AudioManager::setMinVolumeDifferenceDbBeforeStartingRecord(int minVolumeDifferenceDbBeforeStartingRecord) {
    if (m_minVolumeDifferenceDbBeforeStartingRecord == minVolumeDifferenceDbBeforeStartingRecord)
        return;

    m_minVolumeDifferenceDbBeforeStartingRecord = minVolumeDifferenceDbBeforeStartingRecord;
    emit minVolumeDifferenceDbBeforeStartinRecordChanged(m_minVolumeDifferenceDbBeforeStartingRecord);
}

void AudioManager::startPlayingRecordedFile() {
    mDetectionMode = paused;

    Q_ASSERT(mAudioOutputCheckerTimer.data());
    mAudioOutputCheckerTimer->start(audioCheckerCheckIntervalMs);
    mAudioGenerator->sendAudioData(mAudioPlaybackData);
    mAudioOutput->start(mAudioGenerator.data());
    mAudioPlaybackVolumeController->start(defaultVolumeAdjIntervalMs);
    mAudioPlaybackData.clear();
}

void AudioManager::stopPlayingRecordedFile() {
    mDetectionMode = paused;
    mAudioPlaybackVolumeController->stop();
    Q_ASSERT(mAudioOutputCheckerTimer.data());
}

void AudioManager::onAudioInputNotify() {}

bool AudioManager::audioFormatIsOk(const QAudioFormat &format) {

    bool ret = false;

    if (format.sampleSize() == desiredSampleSizeBits &&
        format.sampleType() == QAudioFormat::SignedInt &&
        format.byteOrder() == QAudioFormat::LittleEndian &&
        format.channelCount() == desiredChannelCount) {
        ret = true;
    }

    return ret;
}

void AudioManager::onNewAudioDataReceived(const QByteArray &audioData) {

    Q_ASSERT(audioFormatIsOk(m_format));

    if (isAudioGood() && audioData.size() && audioFormatIsOk(m_format) &&
        mDetectionMode != paused) {

        QAudioBuffer audio(audioData, m_format);
        /* Find current signal max from samples. There should enough samples per slot invocation. Check samples from
         * both channels. */

        sampleDataType_t currentLevel = getCurrentAudioLevel(audio);

        const int minVolumeAdjLevelThreshold = 1;

        /* When audio is reopened initial audio level is 1. This will make min audio level very
         * low. To prevent it, allow only values greater than  minVolumeAdjLevelThreshold. */

        if (currentLevel > minVolumeAdjLevelThreshold) {

            mCurrentMinLevel = static_cast<sampleDataType_t>(
                    qMin<qreal>(mCurrentMinLevel, currentLevel));
            mCurrentMaxLevel = static_cast<sampleDataType_t>(
                    qMax<qreal>(mCurrentMaxLevel, currentLevel));

            mCurrentLevel = static_cast<sampleDataType_t>(currentLevel);

            if (mCurrentLevel > mCurrentMaxLevel) {
                /* Adjust if current level is higher after decay. */
                mCurrentMaxLevel = mCurrentLevel;
            }

            qreal thresholdValue =
                    (calculateDb(static_cast<sampleDataType_t>(mCurrentMinLevel)) +
                     calculateDb(static_cast<sampleDataType_t>(mCurrentMaxLevel))) /
                    2.0;

            switch (mDetectionMode) {

                case paused:
                    /* We should be now playing data. Check if clause beginning of function. */
                    Q_ASSERT(0);
                    break;

                case detectingRecordingTrigger:
                    handleDetectingOfRecordTrigger(audioData, thresholdValue);
                    break;

                case recordingData:
                    handleRecordingOfData(audioData, thresholdValue);
                    break;

                case recordingDataAndDetectingSilenceAfterEndingPlayingTestingThatSilenceIsLongEnough:
                    handleDetectSilenceAfterEndingPlay(audioData, thresholdValue);
                    break;
            }
        }

        emit audioLevelUpdate(
                static_cast<int>(calculateDb(static_cast<sampleDataType_t>(mCurrentLevel))),
                static_cast<int>(calculateDb(static_cast<sampleDataType_t>(mCurrentMinLevel))),
                static_cast<int>(calculateDb(static_cast<sampleDataType_t>(mCurrentMaxLevel))));

    }
}

AudioManager::sampleDataType_t
AudioManager::getCurrentAudioLevel(const QAudioBuffer &audio) const {

    AudioManager::sampleDataType_t ret = 0;

    const auto *data = audio.constData<sampleDataType_t>();

    for (int sample = 0; sample < audio.sampleCount();
         sample++) {
        auto i = qFromLittleEndian<sampleDataType_t>(&data[sample]);

        ret = static_cast<sampleDataType_t>(
                qMax<sampleDataType_t>(ret, qAbs<sampleDataType_t>(i)));
    }
    return ret;
}

void AudioManager::onSoundSilenceCheckTimerTimeOut() {
    mSoundSilenceCheckTimer->stop();
    mAudioPlaybackDataDuringSilenceCheck.clear();
    mDetectionMode = paused;
    emit silenceDetected();
}

void AudioManager::handleDetectSilenceAfterEndingPlay(const QByteArray &audioData, qreal thresholdValue) {

    mAudioPlaybackDataDuringSilenceCheck.append(audioData);

    if (calculateDb(static_cast<sampleDataType_t>(mCurrentLevel)) >
        thresholdValue) {
        /* Sound level raised above threshold. Silence was not long enough.
         * Playing continues. */
        mAudioPlaybackData.append(
                mAudioPlaybackDataDuringSilenceCheck);

        mAudioPlaybackDataDuringSilenceCheck.clear();

        mDetectionMode = recordingData;
        mSoundSilenceCheckTimer->stop();
    } else {
        /* Timer is running. */
    }
}

void AudioManager::handleDetectingOfRecordTrigger(const QByteArray &audioData,
                                                  qreal thresholdValue) {
    adjustLevels();

    /* Waiting rising edge. Then we store data for playback */
    auto diff = calculateDb(static_cast<sampleDataType_t>(mCurrentMaxLevel)) -
                calculateDb(static_cast<sampleDataType_t>(mCurrentMinLevel));

    if (diff > minVolumeDifferenceDbBeforeStartingRecord()
        && calculateDb(static_cast<sampleDataType_t>(mCurrentLevel)) >
           thresholdValue) {
        /* Sound level raised above threshold */
        mAudioPlaybackData.clear();
        mAudioPlaybackData.append(audioData);
        mDetectionMode = recordingData;
        emit soundDetected();
    }
}

void AudioManager::handleRecordingOfData(const QByteArray &audioData, qreal thresholdValue) {

    mAudioPlaybackData.append(audioData); // adding blocks. Should perform.

    /* Recording in progress. If audio level drops, start timer that checks
     * that silence was long enough */

    if (calculateDb(static_cast<sampleDataType_t>(mCurrentLevel)) <
        thresholdValue) {
        /* Sound level dropped below threshold */
        mDetectionMode =
                recordingDataAndDetectingSilenceAfterEndingPlayingTestingThatSilenceIsLongEnough;

        mSoundSilenceCheckTimer->start(minSilenceTimeMs);
    } else {
        if (mAudioPlaybackData.size() > maxAudioBufferLengthBytes(maxRecordTimeSecs, m_format)) {
            /* Keep last n bytes in memory */
            mAudioPlaybackData = mAudioPlaybackData.mid(
                    mAudioPlaybackData.size() -
                    maxAudioBufferLengthBytes(maxRecordTimeSecs, m_format));
        }
    }
}

void AudioManager::adjustLevels() {

    mCurrentMaxLevel = static_cast<sampleDataType_t>(
            static_cast<qreal>(mCurrentMaxLevel) * (1.0 - maxLevelDecayFactor)); // decay a bit.
    mCurrentMinLevel = static_cast<sampleDataType_t>(
            static_cast<qreal>(mCurrentMinLevel) * (1.0 + minLevelIncreaseFactor)); // Increase a bit

    if (mCurrentLevel > mCurrentMaxLevel) {
        /* Adjust if current level is higher after decay. */
        mCurrentLevel = mCurrentMaxLevel;
    }
}

void AudioManager::onAudioOutputNotify() {
}

void AudioManager::onAudioOutputStateChanged(QAudio::State state) {
    if (state == QAudio::IdleState) {
        setMBytesAvailableForPlayback(0);
        mAudioOutputCheckerTimer->stop();
        emit playingRecordedAudioStopped();
    } else if (state == QAudio::ActiveState) {
        emit playingRecordedAudioStarted();
    } else if (state == QAudio::StoppedState) {
        /* No action */
    }
}

void AudioManager::onAudioInputStateChanged(QAudio::State state) {
    // Currently no actions.
}

void AudioManager::onPlayBackVolumeTimerAdjustTimeOut() {
    if (mAudioOutput) {

        if (mAudioGenerator->volumeFactor() < 1.0) {
            const qreal defaultVolumeAdjStepN = 0.1;
            mAudioGenerator->setVolumeFactor(mAudioGenerator->volumeFactor() + defaultVolumeAdjStepN);
        } else {
            mAudioPlaybackVolumeController->stop();
        }
    }
}

bool AudioManager::createAudioInput() {

    bool ret = false;

    mAudioReceiver =
            QSharedPointer<AudioRecorder>(new AudioRecorder(m_format, this));

    mAudioInput =
            QSharedPointer<QAudioInput>::create(mAudioInputDevice, m_format, this);

    mAudioInput->setNotifyInterval(audioNotifyIntervalTargetMs);

    mAudioInput->setBufferSize(rxBufferSizeSec * m_format.sampleRate() *
                               m_format.bytesPerFrame());

    connect(mAudioInput.data(), &QAudioInput::notify, this,
            &AudioManager::onAudioInputNotify);

    connect(mAudioReceiver.data(), &AudioRecorder::newAudioDataReceived, this,
            &AudioManager::onNewAudioDataReceived);

    connect(mAudioInput.data(), &QAudioInput::stateChanged, this,
            &AudioManager::onAudioInputStateChanged);

    connect(mAudioReceiver.data(), &AudioRecorder::audioBytesProcessed, this,
            &AudioManager::onAudioInputBytesProcessed);

    mAudioReceiver->start();
    mAudioInput->start(mAudioReceiver.data());

    if (mAudioInput->state() == QAudio::ActiveState) {
        ret = true;
        emit audioInputDeviceSelected(mAudioInputDevice.deviceName());
    }
    return ret;
}

bool AudioManager::createAudioOutput() {
    bool ret = false;
    mAudioGenerator = QSharedPointer<AudioGenerator>::create(m_format, this);

    mAudioOutput = QSharedPointer<QAudioOutput>(
            new QAudioOutput(mAudioOutputDevice, m_format, this));

    connect(mAudioOutput.data(), &QAudioOutput::notify, this,
            &AudioManager::onAudioOutputNotify);

    connect(mAudioOutput.data(), &QAudioOutput::stateChanged, this,
            &AudioManager::onAudioOutputStateChanged);

    connect(mAudioGenerator.data(), &AudioGenerator::audioBytesProcessed, this,
            &AudioManager::onAudioOutputBytesProcessed);

    mAudioOutput->setNotifyInterval(audioNotifyIntervalTargetMs);

    mAudioOutput->setVolume(1.0);
    mAudioOutput->setBufferSize(txBufferSizeSec * m_format.sampleRate() *
                                m_format.bytesPerFrame());

    mAudioGenerator->start(); // open

    mAudioOutput->start(mAudioGenerator.data());

    if (mAudioOutput->state() == QAudio::ActiveState) {
        ret = true;
        emit audioOutputDeviceSelected(mAudioOutputDevice.deviceName());
    }
    return ret;
}

bool AudioManager::initializeAudio() {
    bool ret = false;
    if (selectAudioDevices()) {
        if (!mAudioInputDevice.isNull() && !mAudioOutputDevice.isNull()) {
            if (setupAudioFormat()) {
                QAudioDeviceInfo outPutInfo(mAudioOutputDevice);
                if (outPutInfo.isFormatSupported(m_format)) {
                    if (createAudioOutput() &&
                        createAudioInput()) {
                        ret = true;
                    }
                }
            }
        }
    }
    return ret;
}

bool AudioManager::setupAudioFormat() {

    QList<int> rates =
            QAudioDeviceInfo::defaultOutputDevice().supportedSampleRates();

    int sampleRate = 0;
    for (auto rate : rates) {

        if (rate > minimumDesiredRateHz) {
            sampleRate = rate;
            break;
        }
    }

    m_format.setSampleRate(sampleRate);
    m_format.setChannelCount(defaultChannelCount);
    m_format.setSampleSize(audioSampleSizeInBits);
    QString codec =
            QAudioDeviceInfo::defaultOutputDevice().supportedCodecs().first();
    m_format.setCodec(codec);
    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setSampleType(QAudioFormat::SignedInt);
    bool ret = audioFormatIsOk(m_format);
    Q_ASSERT(ret);
    return ret;
}

qreal AudioManager::calculateDb(sampleDataType_t signalLevel) {
    auto ref = static_cast<qreal>((signalLevel + 1.0) / std::numeric_limits<sampleDataType_t>::max());
    qreal ret = 20 * log10(qAbs<qreal>(ref));
    Q_ASSERT(ret > -100);
    return ret;
}

void AudioManager::closeAudioDevices() {
    stop();
}

void AudioManager::openAudioDevices() {
    start();
}

void AudioManager::onAudioInputBytesProcessed(int blockSize) {

    mAudioInputBytesReceivedSinceLastCheck += blockSize;

    if (mDetectionMode == recordingData ||
        mDetectionMode == recordingDataAndDetectingSilenceAfterEndingPlayingTestingThatSilenceIsLongEnough) {
        auto v = getMBytesAvailableForPlayback();

        if (v + blockSize < maxAudioBufferLengthBytes(maxRecordTimeSecs, m_format)) {
            v += blockSize;
            setMBytesAvailableForPlayback(v);
        }
    }
    reportSecondsAvailableForPlayback(getMBytesAvailableForPlayback());
}

void AudioManager::onAudioOutputBytesProcessed(int blockSize) {
    mAudioOutputBytesReceivedSinceLastCheck += blockSize;
    auto v = getMBytesAvailableForPlayback();
    v -= blockSize;
    if (v - blockSize < 0) {
        /* Due QAudio 'byte2time and time2byte' functions rounds,
         * it may be that total amount of data is not divisible by blockSize. */
        v = 0;
    }
    setMBytesAvailableForPlayback(v);
    reportSecondsAvailableForPlayback(getMBytesAvailableForPlayback());
    mAudioOutputCheckerTimer->start(audioCheckerCheckIntervalMs);
}

int AudioManager::getMBytesAvailableForPlayback() const {
    return mBytesAvailableForPlayback;
}

void AudioManager::setMBytesAvailableForPlayback(int bytesAvailableForPlayback) {
    AudioManager::mBytesAvailableForPlayback = bytesAvailableForPlayback;
}

void AudioManager::reportSecondsAvailableForPlayback(int bytesAvailableInPlaybackBuffer) {
    qreal seconds = (m_format.durationForBytes(bytesAvailableInPlaybackBuffer) / 1000000.0);
    emit secondsAvailableForPlaybackChanged(seconds);
}

int AudioManager::maxAudioBufferLengthBytes(int secondsToRecordMax, const QAudioFormat &currentRecordFormat) {
    return currentRecordFormat.bytesForDuration(secondsToRecordMax * 1000000);
}

void AudioManager::onAudioInputCheckerTimerTimeout() {
    /* Audio input is always on. If stream is interrupted, there is a problem */
    if (mAudioInputBytesReceivedSinceLastCheck > 0) {
        mAudioInputBytesReceivedSinceLastCheck = 0;
    } else {
        emit audioRecordingError("No audio bytes received");
        setAudioIsGood(false);
    }
}

void AudioManager::onAudioOutputCheckerTimerTimeout() {
    /* Audio input is always on. If stream is interrupted, there is a problem */
    if (mAudioOutputBytesReceivedSinceLastCheck > 0) {
        mAudioOutputBytesReceivedSinceLastCheck = 0;
    } else {
        /* Audio output timer is controlled based on playback. It is not always on */
        mAudioOutputCheckerTimer->stop();
        emit audioRecordingError("No audio bytes received");
        setAudioIsGood(false);
    }

}

bool AudioManager::isAudioGood() const {
    return mAudioIsGood;
}

void AudioManager::setAudioIsGood(bool audioGood) {
    if (mAudioIsGood != audioGood) {
        if (audioGood) {
            emit audioIsGood();
            mSoundInitRetryTimer->stop();
        } else {
            emit audioIsNotGood();
            mSoundInitRetryTimer->start(defaultAudioRetryTimeOutMs);
        }
    }
    mAudioIsGood = audioGood;
}

void AudioManager::onSoundInitRetryTimerTimeOut() {
    if (isAudioGood()) {
        mSoundInitRetryTimer->stop();
    } else {
        if (setupAudio()) {
            mSoundInitRetryTimer->stop();
            resetAudioLevels();

            mDetectionMode = paused;
            emit returnedToPausedState();
        }
    }
}

void AudioManager::resetAudioLevels() {
    mCurrentLevel = 0;
    mCurrentMaxLevel = 0;
    mCurrentMinLevel = std::numeric_limits<sampleDataType_t>::max(); // set max to initial one.
}

#pragma clang diagnostic pop
