/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include "audiogenerator.h"

AudioGenerator::AudioGenerator(const QAudioFormat &format, QObject *parent)
        : QIODevice(parent), mCurrentFormat(format) {}

void AudioGenerator::start() {
    open(QIODevice::ReadOnly);
    mReadInterval.start();
}

void AudioGenerator::stop() { close(); }

qint64 AudioGenerator::readData(char *data, qint64 len) {
    qint64 ret = 0;
    if (mAudioToBeMixed.size() > 0) {
        if (len > mAudioToBeMixed.size()) {
            memcpy(data, mAudioToBeMixed.constData(),
                   static_cast<size_t>(mAudioToBeMixed.size()));
            ret = mAudioToBeMixed.size();
            mAudioToBeMixed.clear();
        } else {
            memcpy(data, mAudioToBeMixed.constData(), static_cast<size_t>(len));
            mAudioToBeMixed = mAudioToBeMixed.mid(static_cast<int>(len), -1);
            ret = len;
        }
        performMonoMix(data, len);
        emit audioBytesProcessed(ret);
    } else {
        emit allPlayed();
    }
    return ret;
}

qint64 AudioGenerator::writeData(const char *data, qint64 len) {

    Q_UNUSED(data)
    Q_UNUSED(len)
    return 0;
}

qint64 AudioGenerator::bytesAvailable() const {
    return mAudioToBeMixed.size() + QIODevice::bytesAvailable();
}

void AudioGenerator::sendAudioData(const QByteArray &audioData) {
    mAudioToBeMixed += audioData;
}

void AudioGenerator::performMonoMix(char *data, qint64 len) {
    /* Check amount of channels being use. Then perform mix.
     * Sum audio samples and divide by channel count and write back */
    auto bytesPerSample = mCurrentFormat.sampleSize() / 8;
    auto channels = mCurrentFormat.channelCount();
    qint64 currentDataPos = 0;

    if (len >= (bytesPerSample * channels)) {

        while (currentDataPos < len) {
            auto mixStartPos = currentDataPos;

            qint32 sampleValue = 0;

            /* Collect one sample per channel. Sum samples. Divide by 2 to ensure not to clip */
            for (int chSample = 0; chSample < channels; chSample++) {
                if (bytesPerSample == 1) {
                    sampleValue += *(reinterpret_cast<qint8 *>(data + currentDataPos));
                } else if (bytesPerSample == 2) {
                    sampleValue += *(reinterpret_cast<qint16 *>(data + currentDataPos));
                } else {
                    Q_ASSERT(bytesPerSample <= 2);
                }
                /* Advance one sample */
                currentDataPos += bytesPerSample;
            }

            sampleValue = sampleValue * mVolumeFactor;

            /* Write new value to both channels */
            qint64 currentMixPos = mixStartPos;
            for (int chSample = 0; chSample < channels; chSample++) {
                if (bytesPerSample == 1) {
                    *(reinterpret_cast<qint8 *>(data + currentMixPos)) = static_cast<qint8 >(sampleValue);
                } else if (bytesPerSample == 2) {
                    *(reinterpret_cast<qint16 *>(data + currentMixPos)) = static_cast<qint16 >(sampleValue);
                } else {
                    Q_ASSERT(bytesPerSample <= 2);
                }
                currentMixPos += bytesPerSample;
            }
            Q_ASSERT(currentMixPos == currentDataPos);
        }
    }
}

qreal AudioGenerator::volumeFactor() {
    Q_ASSERT(mVolumeFactor >= 0 && mVolumeFactor <= 1.0);
    return mVolumeFactor;
}

void AudioGenerator::setVolumeFactor(qreal factor) {
    Q_ASSERT(factor >= 0 && factor <= 1.0);
    mVolumeFactor = factor;
}
