/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include <QAudioEncoderSettings>
#include <QAudioFormat>
#include <QAudioInput>
#include <QAudioOutput>
#include <QAudioBuffer>
#include <QBuffer>
#include <QElapsedTimer>
#include <QObject>
#include <QSharedPointer>
#include <QSound>
#include <QTimer>
#include <QUrl>
//
#include "audiogenerator.h"
#include "audiorecorder.h"

class AudioManager : public QObject {
Q_OBJECT
public:

    typedef qint16 sampleDataType_t;

    explicit AudioManager(QObject *parent = nullptr);

    ~AudioManager() override = default;

    Q_PROPERTY(int minVolumeDifferenceDbBeforeStartingRecord
                       READ
                               minVolumeDifferenceDbBeforeStartingRecord
                       WRITE
                               setMinVolumeDifferenceDbBeforeStartingRecord
                       NOTIFY
                       minVolumeDifferenceDbBeforeStartinRecordChanged)

    int minVolumeDifferenceDbBeforeStartingRecord() const;

signals:

    void audioInputDeviceSelected(QString deviceName);

    void audioIsGood();

    void audioIsNotGood();

    void audioLevelUpdate(int currentDb, int currentMinDb, int currentMaxDb);

    void audioOutputDeviceSelected(QString deviceName);

    void audioPlaybackError(QString errorText);

    void audioRecordingError(QString errorText);

    void minVolumeDifferenceDbBeforeStartinRecordChanged(int minVolumeDifferenceDbBeforeStartinRecord);

    void playingRecordedAudioStarted();

    void playingRecordedAudioStopped();

    void recordingStarted();

    void returnedToPausedState();

    void secondsAvailableForPlaybackChanged(qreal secondsAvailable);

    void silenceDetected();

    void soundDetected();

public slots:

    void closeAudioDevices();

    void openAudioDevices();

    void setMinVolumeDifferenceDbBeforeStartingRecord(int minVolumeDifferenceDbBeforeStartingRecord);

    void start();

    void startPlayingRecordedFile();

    void startRecording();

    void stop();

    void stopPlayingRecordedFile();

    void stopRecording();

private slots:

    static void onAudioInputNotify();

    static void onAudioInputStateChanged(QAudio::State state);

    static void onAudioOutputNotify();

    void onAudioInputBytesProcessed(int blockSize);

    void onAudioOutputBytesProcessed(int blockSize);

    void onAudioInputCheckerTimerTimeout();

    void onAudioOutputCheckerTimerTimeout();

    void onAudioOutputStateChanged(QAudio::State state);

    void onNewAudioDataReceived(const QByteArray &audioData);

    void onPlayBackVolumeTimerAdjustTimeOut();

    void onSoundInitRetryTimerTimeOut();

    void onSoundSilenceCheckTimerTimeOut();

private:
    //
    enum silenceDetectionMode {
        detectingRecordingTrigger,
        recordingData,
        recordingDataAndDetectingSilenceAfterEndingPlayingTestingThatSilenceIsLongEnough,
        paused,
    } mDetectionMode = detectingRecordingTrigger;
    //
    bool mAudioIsGood = true;
    const int audioCheckerCheckIntervalMs = 1000;
    const int audioNotifyIntervalTargetMs = 100;
    const int audioSampleSizeInBits = 16;
    const int defaultAudioRetryTimeOutMs = 1000;
    const int defaultChannelCount = desiredChannelCount;
    const int defaultVolumeAdjIntervalMs = audioNotifyIntervalTargetMs;
    const int maxRecordTimeSecs = 60;
    const int minSilenceTimeMs = 1000;
    const int minimumDesiredRateHz = 40000;
    const int rxBufferSizeSec = 3;
    const int txBufferSizeSec = 3;
    const qreal maxLevelDecayFactor = .01;
    const qreal minLevelIncreaseFactor = maxLevelDecayFactor;
    static const int desiredChannelCount = 2;
    static const int desiredSampleSizeBits = 16;
    //
    QAudioDeviceInfo mAudioInputDevice;
    QAudioDeviceInfo mAudioOutputDevice;
    QAudioFormat m_format;
    QByteArray mAudioPlaybackData;
    QByteArray mAudioPlaybackDataDuringSilenceCheck;
    QSharedPointer<AudioGenerator> mAudioGenerator;
    QSharedPointer<AudioRecorder> mAudioReceiver;
    QSharedPointer<QAudioInput> mAudioInput;
    QSharedPointer<QAudioOutput> mAudioOutput;
    QSharedPointer<QTimer> mAudioInputCheckerTimer;
    QSharedPointer<QTimer> mAudioOutputCheckerTimer;
    QSharedPointer<QTimer> mAudioPlaybackVolumeController;
    QSharedPointer<QTimer> mSoundInitRetryTimer;
    QSharedPointer<QTimer> mSoundSilenceCheckTimer;
    QUrl mLocalFile;
    int mAudioInputBytesReceivedSinceLastCheck = 0;
    int mAudioOutputBytesReceivedSinceLastCheck = 0;
    int mBytesAvailableForPlayback = 0;
    int m_minVolumeDifferenceDbBeforeStartingRecord = 0;
    sampleDataType_t mCurrentLevel = 0;
    sampleDataType_t mCurrentMaxLevel = 0;
    sampleDataType_t mCurrentMinLevel = std::numeric_limits<sampleDataType_t>::max(); // set max to initial one.

    bool isAudioGood() const;

    void setAudioIsGood(bool audioGood);

private:

    bool createAudioInput();

    bool createAudioOutput();

    bool initializeAudio();

    bool selectAudioDevices();

    bool setupAudio();

    bool setupAudioFormat();

    int getMBytesAvailableForPlayback() const;

    sampleDataType_t getCurrentAudioLevel(const QAudioBuffer &audio) const;

    static bool audioFormatIsOk(const QAudioFormat &format);

    static int maxAudioBufferLengthBytes(int secondsToRecordMax, const QAudioFormat &currentRecordFormat);

    static qreal calculateDb(sampleDataType_t signalLevel);

    void adjustLevels();

    void createTimers();

    void handleDetectSilenceAfterEndingPlay(const QByteArray &audioData, qreal thresholdValue);

    void handleDetectingOfRecordTrigger(const QByteArray &audioData, qreal thresholdValue);

    void handleRecordingOfData(const QByteArray &audioData, qreal thresholdValue);

    void removeTimers();

    void reportSecondsAvailableForPlayback(int bytesAvailableInPlaybackBuffer);

    void resetAudioLevels();

    void setMBytesAvailableForPlayback(int bytesAvailableForPlayback);

    void shutdownAudio();
};

#endif // AUDIOMANAGER_H

#pragma clang diagnostic pop