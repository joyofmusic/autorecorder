/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include <qendian.h>
//
#include <QDebug>
//
#include "audiorecorder.h"

AudioRecorder::AudioRecorder() = default;

AudioRecorder::AudioRecorder(const QAudioFormat &format, QObject *parent)
        : QIODevice(parent), m_format(format) {
}

void AudioRecorder::start() {
    open(QIODevice::WriteOnly);
}

void AudioRecorder::stop() {
    close();
}

qint64 AudioRecorder::readData(char *data, qint64 maxLen) {
    Q_UNUSED(data)
    Q_UNUSED(maxLen)
    return 0;
}

qint64 AudioRecorder::writeData(const char *data, qint64 len) {
    QByteArray audioData(data, static_cast<int>(len));
    emit newAudioDataReceived(audioData);
    emit audioBytesProcessed(audioData.size());
    return len;
}

