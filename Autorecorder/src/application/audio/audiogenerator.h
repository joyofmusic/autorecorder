/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef AUDIOGENERATOR_H
#define AUDIOGENERATOR_H

#include <QAudioFormat>
#include <QByteArray>
#include <QDebug>
#include <QElapsedTimer>
#include <QIODevice>
#include <QObject>
#include <QVector>
#include <QtEndian>

class AudioGenerator : public QIODevice {
Q_OBJECT
public:

    AudioGenerator(const QAudioFormat &format, QObject *parent);

    qint64 bytesAvailable() const override;

    qint64 readData(char *data, qint64 len) override;

    qint64 writeData(const char *data, qint64 len) override;

    qreal volumeFactor();

    void sendAudioData(const QByteArray &audioData);

    void setVolumeFactor(qreal factor);

    void start();

    void stop();

    ~AudioGenerator() override = default;

signals:

    void audioBytesProcessed(int blockSize);

    void allPlayed();

private:

    QAudioFormat mCurrentFormat;
    QElapsedTimer mReadInterval;
    QByteArray mAudioToBeMixed;
    qreal mVolumeFactor = 1.0;

    void performMonoMix(char *data, qint64 len);
};

#endif // AUDIOGENERATOR_H
