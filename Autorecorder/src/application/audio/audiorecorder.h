/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef AUDIORECORDER_H
#define AUDIORECORDER_H

#include <QAudioFormat>
#include <QByteArray>
#include <QIODevice>
#include <QObject>
#include <QTimer>

class AudioRecorder : public QIODevice {
Q_OBJECT
public:

    AudioRecorder();

    AudioRecorder(const QAudioFormat &format, QObject *parent);

    qint64 readData(char *data, qint64 maxLen) override;

    qint64 writeData(const char *data, qint64 len) override;

    void start();

    void stop();

    ~AudioRecorder() override = default;

signals:

    void newAudioDataReceived(QByteArray audioData);

    void audioBytesProcessed(int blockSize);

public slots:

private:

    const QAudioFormat m_format;
};

#endif // AUDIORECORDER_H
