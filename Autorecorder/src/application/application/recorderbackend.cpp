/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include <QGuiApplication>
//
#include "recorderbackend.h"
#include "config.h"

#pragma warning (disable : 4068 ) /* disable unknown pragma warnings */
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

RecorderBackend::RecorderBackend(QObject *parent) : QObject(parent) {

    connect(&mMgr, &AudioManager::silenceDetected, this,
            &RecorderBackend::silenceDetected, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::soundDetected, this,
            &RecorderBackend::soundDetected, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::playingRecordedAudioStopped, this,
            &RecorderBackend::onPlayingRecordedAudioStopped, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::playingRecordedAudioStarted, this,
            &RecorderBackend::playingRecordedAudioStarted, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::audioIsGood, this,
            &RecorderBackend::audioIsGood, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::audioIsNotGood, this,
            &RecorderBackend::audioIsNotGood, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::audioLevelUpdate, this,
            &RecorderBackend::audioLevelUpdate, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::audioInputDeviceSelected, this,
            &RecorderBackend::audioInputDeviceSelected, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::audioOutputDeviceSelected, this,
            &RecorderBackend::audioOutputDeviceSelected, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::secondsAvailableForPlaybackChanged, this,
            &RecorderBackend::secondsAvailableForPlaybackChanged, Qt::QueuedConnection);

    connect(&mMgr, &AudioManager::returnedToPausedState, this,
            &RecorderBackend::returnedToPausedState, Qt::QueuedConnection);

}

void RecorderBackend::start() {
    mClock.start();
    emit amountOfSecondsPractised(mDiary.getMSecondsExercisedSinceStart());
    auto d = dynamic_cast<QGuiApplication *>(QCoreApplication::instance());
    Q_ASSERT(d);
    connect(d, &QGuiApplication::applicationStateChanged,
            this, &RecorderBackend::onApplicationStateChanged);
}

void RecorderBackend::startRecording() {
    mClock.start();
    QMetaObject::invokeMethod(&mMgr, "startRecording", Qt::QueuedConnection);
}

void RecorderBackend::stopRecording() {
    mClock.stop();
    mDiary.addSecondsToTrainingDiary(mClock.getElapsedSecs());
    emit amountOfSecondsPractised(mDiary.getMSecondsExercisedSinceStart());

    QMetaObject::invokeMethod(&mMgr, "stopRecording", Qt::QueuedConnection);
}

void RecorderBackend::startPlayingRecordedFile() {
    mClock.start();
    QMetaObject::invokeMethod(&mMgr, "startPlayingRecordedFile",
                              Qt::QueuedConnection);
}

void RecorderBackend::stopPlayingRecordedFile() {
    mClock.stop();
    mDiary.addSecondsToTrainingDiary(mClock.getElapsedSecs());
    emit amountOfSecondsPractised(mDiary.getMSecondsExercisedSinceStart());

    QMetaObject::invokeMethod(&mMgr, "stopPlayingRecordedFile",
                              Qt::QueuedConnection);

}

QString RecorderBackend::version() {
    QString ret = QString("%1").arg(PROJECT_VER);
    return ret;
}

void RecorderBackend::onPlayingRecordedAudioStopped() {
    mClock.stop();
    mDiary.addSecondsToTrainingDiary(mClock.getElapsedSecs());
    emit amountOfSecondsPractised(mDiary.getMSecondsExercisedSinceStart());
    emit playingRecordedAudioStopped();
}

void RecorderBackend::onApplicationStateChanged(Qt::ApplicationState state) {

    if (state == Qt::ApplicationActive) {
        if (mAppCurrentState == Qt::ApplicationSuspended) {
            QMetaObject::invokeMethod(&mMgr, &AudioManager::start, Qt::QueuedConnection);
        }
    } else if (state == Qt::ApplicationSuspended) {
        QMetaObject::invokeMethod(&mMgr, &AudioManager::stop, Qt::QueuedConnection);
    } else {
        /* Other states are ignored */
    }
    mAppCurrentState = state;
}

#pragma clang diagnostic pop