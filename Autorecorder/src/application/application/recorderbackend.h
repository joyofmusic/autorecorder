/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef RECORDERBACKEND_H
#define RECORDERBACKEND_H

#include <QMetaObject>
#include <QObject>
#include <QThread>
#include <QElapsedTimer>
//
#include "audiomanager.h"
#include "practiceDiary.h"
#include "clock.h"

class RecorderBackend : public QObject {
Q_OBJECT
public:
    explicit RecorderBackend(QObject *parent = nullptr);

    ~RecorderBackend() override = default;

    Q_INVOKABLE static QString version();

signals:

    void amountOfSecondsPractised(quint32 timePractised);

    void audioInputDeviceSelected(QString deviceName);

    void audioIsGood();

    void audioIsNotGood();

    void audioLevelUpdate(int currentDb, int currentMinDb, int currentMaxDb);

    void audioOutputDeviceSelected(QString deviceName);

    void playingRecordedAudioStarted();

    void playingRecordedAudioStopped();

    void returnedToPausedState();

    void secondsAvailableForPlaybackChanged(int secondsAvailable);

    void silenceDetected();

    void soundDetected();

public slots:

    void start();

    void startPlayingRecordedFile();

    void startRecording();

    void stopRecording();

    void stopPlayingRecordedFile();

    void onPlayingRecordedAudioStopped();

private slots:

    void onApplicationStateChanged(Qt::ApplicationState state);

private:

    AudioManager mMgr;
    PracticeDiary mDiary;
    Clock mClock;
    Qt::ApplicationState mAppCurrentState = Qt::ApplicationSuspended;
};

#endif // RECORDERBACKEND_H
