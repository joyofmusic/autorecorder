/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

import QtQuick 2.0
import QtQuick.Controls 2.11
import QtQuick.Window 2.2

Item {

    id: root

    property string audioOutputDeviceStr
    property string audioInputDeviceStr
    property string audioLevelsStr
    property string audioStateStr
    property int secsPractised: 0
    property int secsAvailableForPlayback: 0

    function timeLeftStr() {
        let ret = ""
        if (audioStateStr!=="" ) {
            ret = audioStateStr==="Ready for recording"?"":" ("+secsAvailableForPlayback+" s)"
        }
        else {
            ret =""
        }

        return ret
    }

    Rectangle {
        anchors.fill: parent

        id: rootBox
        color:audioStateStr==="Ready for recording"?"lightgreen":"white"

        Column {
            anchors.centerIn: parent
            Text {
                id: timePractised
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Practice time so far "+(secsPractised/3600).toFixed(2)+ " h"
                font.pixelSize:   Screen.pixelDensity*3
            }
            Text {
                text:audioStateStr + root.timeLeftStr()
                font.pixelSize:   Screen.pixelDensity*10
            }
        }
        Column {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: Screen.pixelDensity*3
            spacing: Screen.pixelDensity*1
            Text {
                id: audioInputDevice
                anchors.horizontalCenter: parent.horizontalCenter
                text: audioInputDeviceStr
                font.pixelSize:   Screen.pixelDensity*3
            }
            Text {
                id: audioOutputDevice
                anchors.horizontalCenter: parent.horizontalCenter
                text: audioOutputDeviceStr
                font.pixelSize:   Screen.pixelDensity*3
            }
            Text {
                id: audioLevels
                visible: audioStateStr==="Playing record"?false:true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: Screen.pixelDensity*1
                text:audioLevelsStr
                font.pixelSize:  Screen.pixelDensity*3
            }
        }
    }
}
