/* The MIT License (MIT)

Copyright (c) 2020 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

import QtQuick 2.12
import QtQuick.Controls 2.11
import QtQuick.Window 2.2
//
import com.herwoodtechnologies.autorecorder.backend 1.0

ApplicationWindow {

    id: window

    visible: true
    width:  Screen.pixelDensity*150
    height:  Screen.pixelDensity*80
    title: qsTr("AutoRecorder v" + backEnd.version() +" (written using Qt)")

    property string audioState: ""

    StackView {

        id: stack

        initialItem: mainView
        anchors.fill: parent
    }

    Component {

        id: mainView

        MainScreen {
            Connections {
                target: backEnd

                onSilenceDetected: {
                }

                onSoundDetected: {
                    audioStateStr="Recording"
                }

                onPlayingRecordedAudioStarted: {
                    audioStateStr="Playing record"
                }

                onPlayingRecordedAudioStopped: {
                    audioStateStr="Ready for recording"
                }

                onReturnedToPausedState: {
                    audioStateStr="Ready for recording"
                }

                onAudioIsNotGood: {
                    window.audioState="Cannot setup audio."
                    stack.push(audioProblemViewComponent)
                }

                onAudioLevelUpdate: {
                    audioLevelsStr = currentDb + "dB (current) " + currentMinDb + "dB (min) " +  currentMaxDb +"dB (max, decayed)" ;
                }

                onAudioInputDeviceSelected: {
                    audioInputDeviceStr ="Input: "+deviceName
                }

                onAudioOutputDeviceSelected: {
                    audioOutputDeviceStr ="Output: "+deviceName
                }

                onAmountOfSecondsPractised: {
                    secsPractised = timePractised
                }

                onSecondsAvailableForPlaybackChanged: {
                    secsAvailableForPlayback = secondsAvailable
                }
            }
        }
    }

    Component {

        id: audioProblemViewComponent

        AudioProblemView {
            Connections {
                target: backEnd

                onAudioIsGood: {
                    stack.pop()
                }
            }
        }
    }

    RecorderBackend {

        id:backEnd

        onSilenceDetected: {
            backEnd.stopRecording()
            backEnd.startPlayingRecordedFile()
        }

        onSoundDetected: {
            audioState="Recording"
        }

        onPlayingRecordedAudioStarted: {
            audioState="Playing record"
        }

        onPlayingRecordedAudioStopped: {
            backEnd.startRecording()
        }
    }

    Component.onCompleted: {
        backEnd.start()
    }
}
