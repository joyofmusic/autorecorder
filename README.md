# AutoRecorder

- CMake project.
- Tested with Qt 5.12.6 & MinGW 7.3 64 bit.
- May work on Linux but not tested.
- Read more about Qt from 'https://www.qt.io/'.

# Compile

- Install CMake.
- Install Qt 5.12.6 & MinGW or newer. 
- With Qt Creator, open CMakelists.txt.
- Configure project with QtCreator.
- Compile debug version.

# Windows installer

- Install NSIS
- Edit CMakelist.txt CPACK_PACKAGE_VENDOR, PROJECT_VERSION_MAJOR,PROJECT_VERSION_MINOR and PROJECT_VERSION_PATCH to suit your settings. 
- Create build configuration 'ReleaseAndDeploy' that uses MinGw compiler. 
- Add custom build step where command is 'cpack' and arguments '-C Release'. Working directory is '%{buildDir}'.
- Compile with created configuration. After successfull compile you should see on build directory something like: Autorecorder-1.0.3-win64.exe and Autorecorder-1.0.3-win64.exe.md5. Check installer content from dir _CPack_Packages.

# Signing Windows installer

- You need use own signing key for that.

# License
- MIT
- Read Qt licences from 'https://www.qt.io/'.